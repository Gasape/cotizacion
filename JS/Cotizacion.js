function Calcular(){
    let valor = document.getElementById('Valor');
    let enganche = document.getElementById('enganche');
    let total = document.getElementById('total');
    let planes = document.getElementById('planes');
    let mensual = document.getElementById('Mensual');

    enganche.value = valor.value * .3;
    // console.log(planes.value);

    if (planes.value == 12) {

        total.value = (valor.value - enganche.value) + ((valor.value - enganche.value)*.125);
    }
    if (planes.value == 18) {

        total.value = (valor.value - enganche.value) + ((valor.value - enganche.value)*.172);
    }
    if (planes.value == 24) {

        total.value = (valor.value - enganche.value) + ((valor.value - enganche.value)*.21);
    }
    if (planes.value == 36) {

        total.value = (valor.value - enganche.value) + ((valor.value - enganche.value)*.26);
    }
    if (planes.value == 48) {

        total.value = (valor.value - enganche.value) + ((valor.value - enganche.value)*.45);
    }
    mensual.value = total.value/planes.value;
}

function Limpiar(){
    let valor = document.getElementById('Valor');
    let enganche = document.getElementById('enganche');
    let total = document.getElementById('total');
    let mensual = document.getElementById('Mensual');

    valor.value = ("");
    enganche.value = ("");
    total.value = ("");
    mensual.value = ("");
}
